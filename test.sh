#! /user/bin/env bash

COMMIT_SHA=$(git rev-parse tags/uat-14.7.18~0)
echo $COMMIT_SHA
BRANCH="$(git branch -r --contains $COMMIT_SHA --format='%(refname:short)')"
echo $BRANCH
TARGET_BRANCH="${BRANCH#origin/}"
STR="${TARGET_BRANCH} hello world"
echo $STR 

git --version